package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func ApprovePixTransfer(api_key string) {
	endpoint := "/baas/pix_transfer_approval"
	method := "POST"
	content_type := "application/json"
	body := map[string]interface{}{
		"pix_transfer_key":                 "0b9aebad-5da6-4051-8ff2-3e4996c3aa61",
		"approver_document_identification": "40452784000103",
	}

	additional_headers := make(map[string]string)
	API_KEY := api_key

	response_header, response_body := QI_Sign_Message(endpoint, method, API_KEY, content_type, additional_headers, body)
	// fmt.Println(response_body)

	payloadBytes, err := json.Marshal(response_body)
	if err != nil {
		fmt.Println(err)
	}
	data := bytes.NewReader(payloadBytes)

	// Call API
	req, err := http.NewRequest(method, "https://api-auth.sandbox.qitech.app"+endpoint, data)
	if err != nil {
		log.Fatalln(err)
	}

	// Set Headers
	for key, value := range response_header {
		req.Header.Set(key, value)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalln(err)
	}

	defer resp.Body.Close()

	// Retrieve Response Body
	results, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	// Retrieve Response Header
	header_results := resp.Header
	if err != nil {
		log.Fatalln(err)
	}
	var p_results bytes.Buffer
	err = json.Indent(&p_results, results, "", "  ")
	if err != nil {
		log.Fatalln(err)
	}

	//fmt.Println(header_results)
	// fmt.Println(header_results["Authorization"][0])
	// fmt.Println(p_results.String())

	json_map := make(map[string]string)
	err_new := json.Unmarshal(results, &json_map)
	if err_new != nil {
		log.Fatalln(err)
	}

	response := QI_Translate_Message(endpoint, method, API_KEY, json_map, header_results)

	for key, value := range response {
		fmt.Printf("%v : %v\n", key, value)
	}
}
