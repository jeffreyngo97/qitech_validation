package main

import (
	"crypto/md5"
	"encoding/hex"
	"io/ioutil"
	"log"
	"runtime"
	"time"

	"github.com/golang-jwt/jwt"
)

func QI_Sign_Message(endpoint, method, api_key, content_type string, additional_headers map[string]string, body map[string]interface{}) (request_header map[string]string, request_body map[string]string) {
	var md5_body string
	request_header = make(map[string]string)
	request_body = make(map[string]string)

	ttl := 30 * time.Minute

	// Read Private Key
	data, err := ioutil.ReadFile("jwtECDSASHA512.pem")
	if err != nil {
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, err)
	}

	
	client_private_key, err := jwt.ParseECPrivateKeyFromPEM(data)
	if err != nil {
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, err)
	}

	if len(body) != 0 {
		// encode jwt
		body_claim := jwt.MapClaims{}
		for i, v := range body {
			body_claim[i] = v
		}

		// Commenting since I cannot add an expiration time to body token
		// body_claim["exp"] = time.Now().UTC().Add(ttl).Unix()
	
		body_token := jwt.NewWithClaims(jwt.SigningMethodES512, body_claim)
		encoded_body_token, err := body_token.SignedString(client_private_key)
		if err != nil {
			_, file, line, _ := runtime.Caller(0)
			log.Fatalln(file, line, err)
		}

		request_body["encoded_body"] = encoded_body_token
		// Create Body Hash
		md5_encode := md5.Sum([]byte(encoded_body_token))
		md5_body = hex.EncodeToString(md5_encode[:])
	}

	// Get Current Date
	loc, _ := time.LoadLocation("UTC")
	current_date := time.Now().In(loc)
	date := string(current_date.Format("Mon, 02 Jan 2006 15:04:05 GMT"))

	string_to_sign := (method + "\n" + md5_body + "\n" + content_type + "\n" + date + "\n" + endpoint)

	claims := make(map[string]string)
	claims["sub"] = api_key
	claims["signature"] = string_to_sign

	// encode header jwt
	header_claim := &jwt.MapClaims{
		"sub":       api_key,
		"signature": string_to_sign,
		"exp":       time.Now().UTC().Add(ttl).Unix(),
	}
	// var encoded_body_token interface{}
	header_token := jwt.NewWithClaims(jwt.SigningMethodES512, header_claim)
	encoded_header_token, err := header_token.SignedString(client_private_key)
	if err != nil {
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, err)
	}
	authorization := "QIT" + " " + api_key + ":" + encoded_header_token
	request_header["AUTHORIZATION"] = authorization
	request_header["API-CLIENT-KEY"] = api_key

	if len(additional_headers) != 0 {
		for k, v := range additional_headers {
			request_header[k] = v
		}
	}

	return request_header, request_body
}
