package main

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"runtime"
	"strings"
	"time"

	"github.com/golang-jwt/jwt"
)

func QI_Translate_Message(endpoint, method, api_key string, response_body map[string]string, response_header http.Header) (body_claims jwt.MapClaims) {
	// Read QiTECH Public Key
	data, err := ioutil.ReadFile("jwtECDSAQIPUBSHA512.pem")
	if err != nil {
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, err)
	}

	QI_public_key, err := jwt.ParseECPublicKeyFromPEM(data)
	if err != nil {
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, err)
	}

	// data, err = ioutil.ReadFile("jwtECDSAPUBSHA512.pem")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// client_public_key, err := jwt.ParseECPublicKeyFromPEM(data)
	// if err != nil {
	// 	log.Fatalln(err)
	// }

	// decode
	var body_token *jwt.Token
	if response_body["encoded_body"] != "" {
		body_token, err = jwt.Parse(response_body["encoded_body"], func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodECDSA); !ok {
				return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
			}
			return QI_public_key, err
		})
	} else {
		fmt.Println(response_header)
		fmt.Println(response_body)
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, "Response Has No Body")
	}
	
	body_claims, ok := body_token.Claims.(jwt.MapClaims)
	if !(ok && body_token.Valid) {
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, err)
	}
	authorization := response_header["Authorization"][0]
	header_api_key := response_header["Api-Client-Key"][0]

	if header_api_key != api_key {
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, "The api_key gathered on message's header does not match the one provided to the function")
	}

	split_authorization := strings.Split(authorization, ":")
	if len(split_authorization) != 2 {
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, "Wrong format for the Authorization header")
	}
	authorization_api_key := strings.Split(split_authorization[0], " ")[1]
	if authorization_api_key != api_key {
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, "The api_key gathered on message's authorization header does not match the one provided to the function")
	}

	header_encoded_token := split_authorization[1]

	// decode header
	header_token, err := jwt.Parse(header_encoded_token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodECDSA); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return QI_public_key, err
	})


	header_claims, ok := header_token.Claims.(jwt.MapClaims)
	if !(ok && header_token.Valid) {
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, err)
	}

	signature := header_claims["signature"].(string)
	
	split_signature := strings.Split(signature, "\n")
	signature_method := split_signature[0]
	signature_md5_body := split_signature[1]
	// signature_content_type = split_signature[2]  // Not validated
	signature_date := split_signature[3]
	signature_endpoint := split_signature[4]

	if signature_endpoint != endpoint {
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, "The 'endpoint' parameter gathered on message's signature does not match the one provided to the function")
	}
	if signature_method != method {
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, "The 'method' parameter gathered on message's signature does not match the one provided to the function")
	}

	// Create Body Hash

	md5_encode := md5.Sum([]byte(response_body["encoded_body"]))
	md5_body := hex.EncodeToString(md5_encode[:])
	if signature_md5_body != md5_body {
		fmt.Println(signature_md5_body, md5_body)
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, "The 'md5_body' parameter gathered on message's signature does not match the one provided to the function")
	}

	utc_now := time.Now().Add(7 * time.Hour)
	conv_signature_date, err := time.Parse("Mon, 02 Jan 2006 15:04:05 GMT", signature_date)
	if err != nil {
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, err)
	}

	// Is the time the request made before the current time?
	valid := conv_signature_date.Before(utc_now)
	if !valid {
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, "Invalid signature timestamp")
	}

	return body_claims
}
