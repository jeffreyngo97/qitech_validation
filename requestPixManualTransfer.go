package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func RequestPixManualTransfer(api_key string) {
	endpoint := "/baas/pix_transfer"
	method := "POST"
	content_type := "application/json"
	source_account := map[string]interface{}{
		"account_branch":        "0001",
		"account_digit":         "8",
		"account_number":        "16271",
		"owner_document_number": "40452784000103",
	}
	target_account := map[string]interface{}{
		"account_branch": "0001",
		"account_digit":  "6",
		"branch_digit":   nil,
		"account_number": "1019902",
		"ispb":           "31872495",
		// "bank_code": "336",
		"owner_document_number": "65322181032",
		"owner_name":            "Steven",
		"target_account_type":   "checking_account",
		"owner_person_type":     "natural",
		"trading_name":          "QITech",
	}
	body := map[string]interface{}{
		"pix_transfer_type":  "manual",
		"source_account":     source_account,
		"target_account":     target_account,
		"transaction_amount": 9000,
		//	"schedule_date":                     string(time.Now().Add(7 * time.Hour).Format("2006-01-02")),
		"requester_document_identification": "40452784000103",
	}

	additional_headers := make(map[string]string)
	API_KEY := api_key

	response_header, response_body := QI_Sign_Message(endpoint, method, API_KEY, content_type, additional_headers, body)
	// fmt.Println(response_body)

	payloadBytes, err := json.Marshal(response_body)
	if err != nil {
		fmt.Println(err)
	}
	data := bytes.NewReader(payloadBytes)

	// Call API
	req, err := http.NewRequest(method, "https://api-auth.sandbox.qitech.app"+endpoint, data)
	if err != nil {
		log.Fatalln(err)
	}

	// Set Headers
	for key, value := range response_header {
		req.Header.Set(key, value)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalln(err)
	}

	defer resp.Body.Close()

	// Retrieve Response Body
	results, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	// Retrieve Response Header
	header_results := resp.Header
	if err != nil {
		log.Fatalln(err)
	}
	var p_results bytes.Buffer
	err = json.Indent(&p_results, results, "", "  ")
	if err != nil {
		log.Fatalln(err)
	}

	json_map := make(map[string]string)
	err_new := json.Unmarshal(results, &json_map)
	if err_new != nil {
		log.Fatalln(err)
	}

	response := QI_Translate_Message(endpoint, method, API_KEY, json_map, header_results)

	for key, value := range response {
		fmt.Printf("%v : %v\n", key, value)
	}

}
