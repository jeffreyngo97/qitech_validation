package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

// Fetch Approved PIX Transaction Receipt
func GetTransferReceipt(api_key, transaction_key string) {
	// Client api-key, provided by QI to client
	API_KEY := api_key
	endpoint := "/transaction_receipt/" + transaction_key + "?pdf=false"
	method := "GET"
	content_type := ""
	body := make(map[string]interface{})
	additional_headers := make(map[string]string)

	response_header, _ := QI_Sign_Message(endpoint, method, API_KEY, content_type, additional_headers, body)

	// Call API
	req, err := http.NewRequest(method, "https://api-auth.sandbox.qitech.app"+endpoint, nil)
	if err != nil {
		log.Fatalln(err)
	}
	for key, value := range response_header {
		req.Header.Set(key, value)
		req.Header.Set(key, value)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalln(err)
	}

	defer resp.Body.Close()

	// Retrieve Response Body
	results, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	// Retrieve Response Header
	header_results := resp.Header

	var p_results bytes.Buffer
	err = json.Indent(&p_results, results, "", "  ")
	if err != nil {
		log.Fatalln(err)
	}
	// fmt.Println(p_results.String())

	json_map := make(map[string]string)
	err_new := json.Unmarshal(results, &json_map)
	if err_new != nil {
		log.Fatalln(err)
	}

	response := QI_Translate_Message(endpoint, method, API_KEY, json_map, header_results)

	for key, value := range response {
		fmt.Printf("%v : %v\n", key, value)
	}

}
