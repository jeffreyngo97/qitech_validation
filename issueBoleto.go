package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

func IssueBoleto(api_key string) {
	endpoint := "/multibank_instruction?use_multi_process=false"
	method := "POST"
	content_type := "application/json"
	occurrences := map[string]interface{}{
		"amount":                              1337,
		"automatic_bankruptcy_protest":        false,
		"bank_teller_instructions":            "Não aceitar após vencimento",
		"beneficiary_account_key":             "953b2ad8-e776-4262-bd5c-d22b91197e14",
		"beneficiary_key":                     "953b2ad8-e776-4262-bd5c-d22b91197e14",
		"days_to_bankruptcy_protest":          0,
		"document_number":                     "442576079",
		"expiration":                          string(time.Now().AddDate(0, 1, 0).Format("2006-01-02")), // Add One Month
		"fine_percentage":                     "3",
		"interest_daily_value":                "0",
		"occurrence_type":                     "registration",
		"payer_address":                       "Rua Carlos Sampaio, 123",
		"payer_document":                      "65322181032",
		"payer_name":                          "João Ninguem",
		"payer_person_type":                   "natural",
		"payer_postal_code_root":              "15800",
		"payer_postal_code_suffix":            "020",
		"printing_policy":                     "no_printing",
		"registration_institution_enumerator": "qi_scd",
		"requester_profile":                   "01",
		"requester_profile_code":              "329-09-0001-0068819",
	}

	// Request has to be nested in an array
	occ_arr := [1]interface{}{occurrences}
	body := map[string]interface{}{
		"occurrences": occ_arr,
	}

	additional_headers := make(map[string]string)
	API_KEY := api_key

	response_header, response_body := QI_Sign_Message(endpoint, method, API_KEY, content_type, additional_headers, body)
	// fmt.Println(response_body)

	payloadBytes, err := json.Marshal(response_body)
	if err != nil {
		fmt.Println(err)
	}
	data := bytes.NewReader(payloadBytes)

	// Call API
	req, err := http.NewRequest(method, "https://api-auth.sandbox.qitech.app"+endpoint, data)
	if err != nil {
		log.Fatalln(err)
	}

	// Set Headers
	for key, value := range response_header {
		req.Header.Set(key, value)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalln(err)
	}

	defer resp.Body.Close()

	// Retrieve Response Body
	results, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	// Retrieve Response Header
	header_results := resp.Header
	if err != nil {
		log.Fatalln(err)
	}
	var p_results bytes.Buffer
	err = json.Indent(&p_results, results, "", "  ")
	if err != nil {
		log.Fatalln(err)
	}

	//fmt.Println(header_results)
	// fmt.Println(header_results["Authorization"][0])
	// fmt.Println(p_results.String())

	json_map := make(map[string]string)
	err_new := json.Unmarshal(results, &json_map)
	if err_new != nil {
		log.Fatalln(err)
	}

	response := QI_Translate_Message(endpoint, method, API_KEY, json_map, header_results)

	// fmt.Println(p_results.String())

	for key, value := range response {
		// value = value.(string)[:(len(value.(string)) - 5)]
		fmt.Printf("%v : %v\n", key, value)

	}

}
