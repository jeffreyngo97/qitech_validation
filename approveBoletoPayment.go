package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"runtime"
)

func ApproveBoletoPayment(api_key string) {
	endpoint := "/bank_slip/payment"
	method := "POST"
	content_type := "application/json"
	body := map[string]interface{}{
		"digitable_line":       "32990001039000000000117006881902189010000133700",
		"resource_account_key": "a069e667-0b54-456e-9358-ab9a75723550",
		// If you would like to schedule the payment
		// "payment_date" : "2022-01-17",
	}

	additional_headers := make(map[string]string)
	API_KEY := api_key

	response_header, response_body := QI_Sign_Message(endpoint, method, API_KEY, content_type, additional_headers, body)
	// fmt.Println(response_body)

	payloadBytes, err := json.Marshal(response_body)
	if err != nil {
		fmt.Println(err)
	}
	data := bytes.NewReader(payloadBytes)

	// Call API
	req, err := http.NewRequest(method, "https://api-auth.sandbox.qitech.app"+endpoint, data)
	if err != nil {
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, err)
	}

	// Set Headers
	for key, value := range response_header {
		req.Header.Set(key, value)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, err)
	}

	defer resp.Body.Close()

	// Retrieve Response Body
	results, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, err)
	}

	// Retrieve Response Header
	header_results := resp.Header
	if err != nil {
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, err)
	}

	var p_results bytes.Buffer
	err = json.Indent(&p_results, results, "", "  ")
	if err != nil {
		fmt.Println(string(results))
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, err)
	}

	// fmt.Println(header_results)
	// fmt.Println(header_results["Authorization"][0])
	// fmt.Println(p_results.String())

	json_map := make(map[string]string)
	err_new := json.Unmarshal(results, &json_map)
	if err_new != nil {
		_, file, line, _ := runtime.Caller(0)
		log.Fatalln(file, line, err)
	}

	response := QI_Translate_Message(endpoint, method, API_KEY, json_map, header_results)

	for key, value := range response {
		fmt.Printf("%v : %v\n", key, value)
	}
}
